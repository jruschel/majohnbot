//------------------------------------------------------------------------------
// Jogador de Futebol MajohnBot - Versao Fuzzy
// Marina Rey (220486) e Joao Paulo Ruschel (219435)
//------------------------------------------------------------------------------
// Inclusao da biblioteca que implementa a interface com o SoccerMatch.
#include "SoccerPlayer_Library/environm.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Macros para calculo de MAX e MIN
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

// Verbose
//#define VERBOSE 

// Indices semanticos para BallAngle_ e TargetAngle_
#define ANGLE_BACKLEFT 0
#define ANGLE_LEFT 1
#define ANGLE_FRONT 2
#define ANGLE_RIGHT 3
#define ANGLE_BACKRIGHT 4

#define ANGLE_SHARP_LEFT 0
#define ANGLE_SLIGHT_LEFT 1
#define ANGLE_FRONT 2
#define ANGLE_SLIGHT_RIGHT 3
#define ANGLE_SHARP_RIGHT 4

//Conjuntos de Entrada
#define INPUT_MAX_ANGLE M_PI
#define INPUT_FRONT_SIZE  15 //Mirrored
#define INPUT_SIDE_SIZE  75 
#define INPUT_BACK_SIZE  90
#define INPUT_FRONT_SIDE_OVERLAP 0.2 //PERCENTAGE of FRONT 0 to 1
#define INPUT_BACK_SIDE_OVERLAP 0.1 //PERCENTAGE of BACK 0 to 1

//Conjuntos de Saida
#define OUTPUT_MAX_TURN M_PI/2
#define OUTPUT_FRONT_SIZE  1 //Mirrored
#define OUTPUT_SLIGHT_SIZE  4
#define OUTPUT_SHARP_SIZE  4
#define OUTPUT_FRONT_SLIGHT_OVERLAP 0.4 //PERCENTAGE of FRONT 0 to 1
#define OUTPUT_SHARP_SLIGHT_OVERLAP 0.2 //PERCENTAGE of SHARP 0 to 1

// Indices semanticos 
#define SPEED_FAST 0
#define SPEED_SLOW 1

// Indices semanticos para BallDistance_
#define DIST_NEAR 0
#define DIST_FAR 1

// Velocidade maxima
#define MAX_ABS_SPEED 1

// Numero de passos (divisoes do espaco) na etapa de defuzzificacao,
//	com o metodo de centroide
#define CENTROID_STEPS 100

// Conjunto Fuzzy Padrao - Trapezio
typedef struct trapezio
{
	float alpha, beta, gamma, delta;	// parametros
									// inicializa o trapezio com os valores passados
	void init(float _alpha, float _beta, float _gamma, float _delta)
	{
		alpha = _alpha;
		beta = _beta;
		gamma = _gamma;
		delta = _delta;
	}
	// calcula o valor f(x) (entre 0 e 1)
	float sample(float x)
	{
		if (x < alpha) return 0;
		if ((x >= alpha) && (x <= beta)) return (x - alpha) / (beta - alpha);
		if ((x > beta) && (x <= gamma)) return 1;
		if ((x > gamma) && (x <= delta)) return (delta - x) / (delta - gamma);
		if (x > delta) return 0;
	}
} Trapezio;

//Variaveis dos Conjuntos de Entrada
float INPUT_COVERAGE = INPUT_BACK_SIZE + INPUT_SIDE_SIZE + INPUT_FRONT_SIZE;

float INPUT_BACK_SIDE_RIGHT_1 = INPUT_MAX_ANGLE*((INPUT_BACK_SIZE/INPUT_COVERAGE) - (INPUT_BACK_SIDE_OVERLAP)*(INPUT_BACK_SIZE)/INPUT_COVERAGE -1);
float INPUT_BACK_SIDE_RIGHT_2= INPUT_MAX_ANGLE*((INPUT_BACK_SIZE/INPUT_COVERAGE) + (INPUT_BACK_SIDE_OVERLAP)*(INPUT_BACK_SIZE)/INPUT_COVERAGE -1);
float INPUT_SIDE_FRONT_RIGHT_1 = INPUT_MAX_ANGLE*( -(INPUT_FRONT_SIZE/INPUT_COVERAGE) -(INPUT_FRONT_SIDE_OVERLAP)*(INPUT_FRONT_SIZE)/INPUT_COVERAGE);
float INPUT_SIDE_FRONT_RIGHT_2 = INPUT_MAX_ANGLE*( -(INPUT_FRONT_SIZE/INPUT_COVERAGE) +(INPUT_FRONT_SIDE_OVERLAP)*(INPUT_FRONT_SIZE)/INPUT_COVERAGE);

float INPUT_BACK_SIDE_LEFT_1 = INPUT_MAX_ANGLE*(-(INPUT_BACK_SIZE/INPUT_COVERAGE) + (INPUT_BACK_SIDE_OVERLAP)*(INPUT_BACK_SIZE)/INPUT_COVERAGE +1);
float INPUT_BACK_SIDE_LEFT_2= INPUT_MAX_ANGLE*(-(INPUT_BACK_SIZE/INPUT_COVERAGE) - (INPUT_BACK_SIDE_OVERLAP)*(INPUT_BACK_SIZE)/INPUT_COVERAGE +1);
float INPUT_SIDE_FRONT_LEFT_1 = INPUT_MAX_ANGLE*((INPUT_FRONT_SIZE/INPUT_COVERAGE) +(INPUT_FRONT_SIDE_OVERLAP)*(INPUT_FRONT_SIZE)/INPUT_COVERAGE);
float INPUT_SIDE_FRONT_LEFT_2 = INPUT_MAX_ANGLE*((INPUT_FRONT_SIZE/INPUT_COVERAGE) -(INPUT_FRONT_SIDE_OVERLAP)*(INPUT_FRONT_SIZE)/INPUT_COVERAGE);


//Variaveis dos Conjuntos de Saida
float OUTPUT_COVERAGE = OUTPUT_SHARP_SIZE + OUTPUT_SLIGHT_SIZE + OUTPUT_FRONT_SIZE;

float OUTPUT_SHARP_SLIGHT_RIGHT_1 = OUTPUT_MAX_TURN*((OUTPUT_SHARP_SIZE/OUTPUT_COVERAGE) - (OUTPUT_SHARP_SLIGHT_OVERLAP)*(OUTPUT_SHARP_SIZE)/OUTPUT_COVERAGE -1);
float OUTPUT_SHARP_SLIGHT_RIGHT_2= OUTPUT_MAX_TURN*((OUTPUT_SHARP_SIZE/OUTPUT_COVERAGE) + (OUTPUT_SHARP_SLIGHT_OVERLAP)*(OUTPUT_SHARP_SIZE)/OUTPUT_COVERAGE -1);
float OUTPUT_SLIGHT_FRONT_RIGHT_1 = OUTPUT_MAX_TURN*( -(OUTPUT_FRONT_SIZE/OUTPUT_COVERAGE) -(OUTPUT_FRONT_SLIGHT_OVERLAP)*(OUTPUT_FRONT_SIZE)/OUTPUT_COVERAGE);
float OUTPUT_SLIGHT_FRONT_RIGHT_2 = OUTPUT_MAX_TURN*( -(OUTPUT_FRONT_SIZE/OUTPUT_COVERAGE) +(OUTPUT_FRONT_SLIGHT_OVERLAP)*(OUTPUT_FRONT_SIZE)/OUTPUT_COVERAGE);

float OUTPUT_SHARP_SLIGHT_LEFT_1 = OUTPUT_MAX_TURN*(-(OUTPUT_SHARP_SIZE/OUTPUT_COVERAGE) + (OUTPUT_SHARP_SLIGHT_OVERLAP)*(OUTPUT_SHARP_SIZE)/OUTPUT_COVERAGE +1);
float OUTPUT_SHARP_SLIGHT_LEFT_2= OUTPUT_MAX_TURN*(-(OUTPUT_SHARP_SIZE/OUTPUT_COVERAGE) - (OUTPUT_SHARP_SLIGHT_OVERLAP)*(OUTPUT_SHARP_SIZE)/OUTPUT_COVERAGE +1);
float OUTPUT_SLIGHT_FRONT_LEFT_1 = OUTPUT_MAX_TURN*((OUTPUT_FRONT_SIZE/OUTPUT_COVERAGE) +(OUTPUT_FRONT_SLIGHT_OVERLAP)*(OUTPUT_FRONT_SIZE)/OUTPUT_COVERAGE);
float OUTPUT_SLIGHT_FRONT_LEFT_2 = OUTPUT_MAX_TURN*((OUTPUT_FRONT_SIZE/OUTPUT_COVERAGE) -(OUTPUT_FRONT_SLIGHT_OVERLAP)*(OUTPUT_FRONT_SIZE)/OUTPUT_COVERAGE);



// Declaracao do objeto que representa o ambiente.
environm::soccer::clientEnvironm environment;



// Inputs e Outputs
float ballAngle, targetAngle, ballDistance;	// inputs
float outAngle, outSpeed;					// outputs
float leftMotor, rightMotor;				// outputs (valores pro robo)
float last_leftMotor = 0, last_rightMotor = 0;

// Matriz com valores de ativacao de regras (inferencia)
float activation_matrix[5][5][2];

// Definicao dos conjuntos fuzzy de entrada
Trapezio BallAngle_Conjunto[5];
Trapezio TargetAngle_Conjunto[5];
Trapezio BallDistance_Conjunto[2];

// Definicao dos conjuntos fuzzy de saida
Trapezio OutAngle_Conjunto[5];
Trapezio OutSpeed_Conjunto[2];

// Valores de ativacao de cada conjunto, a partir da entrada atual
float BallAngle_Valor[5];
float TargetAngle_Valor[5];
float BallDistance_Valor[2];

// Funcoes principais - cada etapa de uma iteracao
void fuzzification();
void inference_defuzzify();
void collisionAvoidance();

// coisas do neural
int ownScore = 0;
int rivalScore = 0;

// Log
const int MAX_LOG_SIZE = 1280;
typedef struct S_LogEntry 
{
    float dist;
    float ballAng;
    float tarAng;
    float coll;
    float obstAng;
    float spin;
    float last_for0;
    float last_for1;
    float for0;
    float for1;
} LogEntry;

LogEntry logEntries[MAX_LOG_SIZE];
int logIndex = 0;
int logSize = 0;

void log_add(float dist, float ballAng, float tarAng, float coll, float obstAng, 
    float spin, float last_for0, float last_for1, float for0, float for1)
{
    logEntries[logIndex].dist = dist;
    logEntries[logIndex].ballAng = ballAng;
    logEntries[logIndex].tarAng = tarAng;
    logEntries[logIndex].coll = coll;
    logEntries[logIndex].obstAng = obstAng;
    logEntries[logIndex].spin = spin;
    logEntries[logIndex].last_for0 = last_for0;
    logEntries[logIndex].last_for1 = last_for1;
    logEntries[logIndex].for0 = for0;
    logEntries[logIndex].for1 = for1;
    logIndex = (logIndex + 1) % MAX_LOG_SIZE;
	
	if (logSize == MAX_LOG_SIZE-1) printf("Log: Reached max!\n");
	
    if (++logSize > MAX_LOG_SIZE) logSize = MAX_LOG_SIZE;
}
void log_reset()
{
    logIndex = 0;
    logSize = 0;
}

void log_saveAndReset()
{
    FILE* logFile;
    logFile = fopen ("log.txt","a");
    int count = 0;
    int i;
    for (i = logIndex; i < logSize; i++) {
        fprintf(logFile,"%f %f %f %f %f %f %f %f %f %f\n",
            logEntries[i].dist,
            logEntries[i].ballAng,
            logEntries[i].tarAng,
            logEntries[i].coll,
            logEntries[i].obstAng,
            logEntries[i].spin,
            logEntries[i].last_for0,
            logEntries[i].last_for1,
            logEntries[i].for0,
            logEntries[i].for1
        );
        count++;
    }
    for (i = 0; i < logIndex; i++) {
        fprintf(logFile,"%f %f %f %f %f %f %f %f %f %f\n",
            logEntries[i].dist,
            logEntries[i].ballAng,
            logEntries[i].tarAng,
            logEntries[i].coll,
            logEntries[i].obstAng,
            logEntries[i].spin,
            logEntries[i].last_for0,
            logEntries[i].last_for1,
            logEntries[i].for0,
            logEntries[i].for1
        );
        count++;
    }

    fclose (logFile);
	
	
    printf("Log: Generated log. Index: %d, Size: %d. Generated %d entries.", logIndex, logSize, count);

    logIndex = 0;
    logSize = 0;
}

// MAIN
int main(int argc, char* argv[])
{
	// Informacoes de endereco e porta da linha de comando
	if (argc != 3)
	{
		printf("\nInvalid parameters. Expecting:");
		printf("\nSoccerPlayer SERVER_ADDRESS_STRING SERVER_PORT_NUMBER\n");
		printf("\nSoccerPlayer localhost 1024\n");
		return 0;
	}

	// Conecta-se ao SoccerMatch. Supoe que SoccerMatch esta rodando na maquina
	// local e que um dos robos esteja na porta 1024. Porta e local podem mudar.
	if (!environment.connect(argv[1], atoi(argv[2])))
	{
		printf("\nFail connecting to the SoccerMatch.\n");
		return 0;  // Cancela operacao se nao conseguiu conectar-se.
	}

	// Configuracao dos conjuntos
	// angulo da bola
	BallAngle_Conjunto[ANGLE_BACKLEFT].init(INPUT_BACK_SIDE_LEFT_2,INPUT_BACK_SIDE_LEFT_1,
                                            INPUT_MAX_ANGLE,INPUT_MAX_ANGLE);
	BallAngle_Conjunto[ANGLE_LEFT].init(INPUT_SIDE_FRONT_LEFT_2,INPUT_SIDE_FRONT_LEFT_1,
                                            INPUT_BACK_SIDE_LEFT_2,INPUT_BACK_SIDE_LEFT_1);
	BallAngle_Conjunto[ANGLE_FRONT].init(INPUT_SIDE_FRONT_RIGHT_1,INPUT_SIDE_FRONT_RIGHT_2,
                                            INPUT_SIDE_FRONT_LEFT_2,INPUT_SIDE_FRONT_LEFT_1);
	BallAngle_Conjunto[ANGLE_RIGHT].init(INPUT_BACK_SIDE_RIGHT_1,INPUT_BACK_SIDE_RIGHT_2,
                                            INPUT_SIDE_FRONT_RIGHT_1,INPUT_SIDE_FRONT_RIGHT_2);
	BallAngle_Conjunto[ANGLE_BACKRIGHT].init(-INPUT_MAX_ANGLE,-INPUT_MAX_ANGLE,
                                            INPUT_BACK_SIDE_RIGHT_1,INPUT_BACK_SIDE_RIGHT_2);

	// angulo do alvo
	TargetAngle_Conjunto[ANGLE_BACKLEFT].init(INPUT_BACK_SIDE_LEFT_2,INPUT_BACK_SIDE_LEFT_1,
                                            INPUT_MAX_ANGLE,INPUT_MAX_ANGLE);
	TargetAngle_Conjunto[ANGLE_LEFT].init(INPUT_SIDE_FRONT_LEFT_2,INPUT_SIDE_FRONT_LEFT_1,
                                            INPUT_BACK_SIDE_LEFT_2,INPUT_BACK_SIDE_LEFT_1);
	TargetAngle_Conjunto[ANGLE_FRONT].init(INPUT_SIDE_FRONT_RIGHT_1,INPUT_SIDE_FRONT_RIGHT_2,
                                            INPUT_SIDE_FRONT_LEFT_2,INPUT_SIDE_FRONT_LEFT_1);
	TargetAngle_Conjunto[ANGLE_RIGHT].init(INPUT_BACK_SIDE_RIGHT_1,INPUT_BACK_SIDE_RIGHT_2,
                                            INPUT_SIDE_FRONT_RIGHT_1,INPUT_SIDE_FRONT_RIGHT_2);
	TargetAngle_Conjunto[ANGLE_BACKRIGHT].init(-INPUT_MAX_ANGLE,-INPUT_MAX_ANGLE,
                                            INPUT_BACK_SIDE_RIGHT_1,INPUT_BACK_SIDE_RIGHT_2);

	// distancia da bola
	BallDistance_Conjunto[DIST_FAR].init(200, 250, 10000, 10000);
	BallDistance_Conjunto[DIST_NEAR].init(0, 0, 200, 250);

	// angulo de saida
	OutAngle_Conjunto[ANGLE_SHARP_LEFT].init(OUTPUT_SHARP_SLIGHT_LEFT_2,OUTPUT_SHARP_SLIGHT_LEFT_1,
                                        OUTPUT_MAX_TURN,OUTPUT_MAX_TURN);
	OutAngle_Conjunto[ANGLE_SLIGHT_LEFT].init(OUTPUT_SLIGHT_FRONT_LEFT_2,OUTPUT_SLIGHT_FRONT_LEFT_1,
                                        OUTPUT_SHARP_SLIGHT_LEFT_2,OUTPUT_SHARP_SLIGHT_LEFT_1);
	OutAngle_Conjunto[ANGLE_FRONT].init(OUTPUT_SLIGHT_FRONT_RIGHT_1,OUTPUT_SLIGHT_FRONT_RIGHT_2,
                                        OUTPUT_SLIGHT_FRONT_LEFT_2,OUTPUT_SLIGHT_FRONT_LEFT_1);
	OutAngle_Conjunto[ANGLE_SLIGHT_RIGHT].init(OUTPUT_SHARP_SLIGHT_RIGHT_1,OUTPUT_SHARP_SLIGHT_RIGHT_2,
                                        OUTPUT_SLIGHT_FRONT_RIGHT_1,OUTPUT_SLIGHT_FRONT_RIGHT_2);
	OutAngle_Conjunto[ANGLE_SHARP_RIGHT].init(-OUTPUT_MAX_TURN,-OUTPUT_MAX_TURN,
                                        OUTPUT_SHARP_SLIGHT_RIGHT_1,OUTPUT_SHARP_SLIGHT_RIGHT_2);

	// velocidade de saida
	OutSpeed_Conjunto[SPEED_FAST].init(0.3, 0.4, 0.6, 0.6);
	OutSpeed_Conjunto[SPEED_SLOW].init(0.1, 0.1, 0.3, 0.4);

	// Laco de execucao de acoes.
	printf("\nRunning...\n");

	while (1)
	{
		// Deve obter os dados desejados do ambiente. Metodos do clientEnvironm.
		// Exemplos de metodos que podem ser utilizados.
		ballAngle = environment.getBallAngle();
		targetAngle = environment.getTargetAngle(environment.getOwnGoal());
		ballDistance = environment.getDistance();

		// fuzzificacao - transforma entrada crisp em conjuntos fuzzy
		fuzzification();

		// aplica as regras de inferencia, e defuzifica (otimizado)
		inference_defuzzify();

		// calcula, a partir do angulo de saida, e da velocidade de movimento,
		//  a forca para cada motor
        //outSpeed = 0.4;
		leftMotor = cos(outAngle) - sin(outAngle);
		rightMotor = cos(outAngle) + sin(outAngle);
		leftMotor = leftMotor * outSpeed;
		rightMotor = rightMotor * outSpeed;

        // evita colisoes
		collisionAvoidance();

		if (leftMotor < -1)
			leftMotor = -1;
		if (leftMotor > 1)
			leftMotor = 1;
		if (rightMotor < -1)
			rightMotor = -1;
		if (rightMotor > 1)
			rightMotor = 1;
        
        // Log
		log_add(ballDistance, ballAngle, targetAngle, environment.getCollision(), environment.getObstacleAngle(), environment.getSpin(),
			last_leftMotor, last_rightMotor, leftMotor, rightMotor);

		last_leftMotor = leftMotor;
		last_rightMotor = rightMotor;

		if(ownScore < environment.getOwnScore())
		{
			//salvar no arquivo
			log_saveAndReset();
			//printf("gol nosso\n");
		}
		ownScore = environment.getOwnScore();
		
		if(rivalScore < environment.getRivalScore())
		{
			// descartar 
			log_reset();
			printf("gol dele\n");
		}
		rivalScore = environment.getRivalScore();
		
		// Transmite acao do robo ao ambiente. Fica bloqueado ate que todos os
		// robos joguem. Se erro, retorna false (neste exemplo, sai do laco).
		if (!environment.act(leftMotor, rightMotor))
		{
			break; // Termina a execucao se falha ao agir.
		}
	}

	return 0;

}
//------------------------------------------------------------------------------

// variaveis e conjuntos fuzzy de entrada
void fuzzification()
{
	int i;
	for (i = 0; i < 5; i++)
		BallAngle_Valor[i] = BallAngle_Conjunto[i].sample(ballAngle);
	for (i = 0; i < 5; i++)
		TargetAngle_Valor[i] = TargetAngle_Conjunto[i].sample(targetAngle);
	for (i = 0; i < 2; i++)
		BallDistance_Valor[i] = BallDistance_Conjunto[i].sample(ballDistance);
    
	#ifdef VERBOSE
	printf("CF Ball Angle: { ");
	for (i = 0; i < 5; i++)
		printf("%.2f, ", BallAngle_Valor[i]);
	printf(" }\n");
	printf("CF Target Angle: { ");
	for (i = 0; i < 5; i++)
		printf("%.2f, ", TargetAngle_Valor[i]);
	printf(" }\n");
	printf("CF Ball Distance: { ");
	for (i = 0; i < 2; i++)
		printf("%.2f, ", BallDistance_Valor[i]);
	printf(" }\n");
	#endif
}

void print_activation_matrix() 
{
	int i, j;
	printf("\nmatrix0: \n ");
	for (i = 0; i < 5; i++)
	{
		for (j = 0; j < 5; j++)
		{
			printf("%.2f ", activation_matrix[i][j][0]);
		}
		printf("\n ");
	}
    
    printf("\nmatrix1: \n ");
	for (i = 0; i < 5; i++)
	{
		for (j = 0; j < 5; j++)
		{
			printf("%.2f ", activation_matrix[i][j][1]);
		}
		printf("\n ");
	}
}

// aplicacao de uma unica regra
void inferenceRule(int ballAngleIndex, int targetAngleIndex, int ballDistanceIndex,
	int angleIndex, int speedIndex, float x_a, float x_s, float *Y_A, float *Y_S)
{
	*Y_A = MAX(MIN(activation_matrix[ballAngleIndex][targetAngleIndex][ballDistanceIndex], OutAngle_Conjunto[angleIndex].sample(x_a)), *Y_A);
	*Y_S = MAX(MIN(activation_matrix[ballAngleIndex][targetAngleIndex][ballDistanceIndex], OutSpeed_Conjunto[speedIndex].sample(x_s)), *Y_S);
}

// regras, variaveis e conjuntos fuzzy internos
void inference_defuzzify()
{
	// matriz de relacao
	int i, j, k;
	for (i = 0; i < 5; i++)
	{
		for (j = 0; j < 5; j++)
		{
			for (k = 0; k < 2; k++)
			{
				activation_matrix[i][j][k] = MIN(MIN(BallAngle_Valor[i], TargetAngle_Valor[j]), BallDistance_Valor[k]);
			}
		}
	}
	#ifdef VERBOSE
	print_activation_matrix();
	#endif

	// inferencia e defuzificacao em paralelo, para otimizacao
	int step;
	float x_a, x_s, Y_A = 0, Y_S = 0;
	float sumXY_A = 0, sumXY_S = 0;
	float sumY_A = 0, sumY_S = 0;

	float mPiStep = M_PI / (float)(CENTROID_STEPS);
	float mSpeedStep = (MAX_ABS_SPEED * 2) / (float)(CENTROID_STEPS);
	for (step = 0; step <= CENTROID_STEPS; step++)
	{
		x_a = (step - (CENTROID_STEPS * 0.5)) * mPiStep;
		x_s = (step - (CENTROID_STEPS * 0.5)) * mSpeedStep;
		Y_A = 0;
		Y_S = 0;

		/* Ball Angle, Target Angle, Ball Distance */
		inferenceRule(ANGLE_BACKRIGHT, ANGLE_BACKRIGHT, DIST_NEAR, ANGLE_SLIGHT_RIGHT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_BACKRIGHT, ANGLE_BACKRIGHT, DIST_FAR, ANGLE_SLIGHT_RIGHT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_BACKRIGHT, ANGLE_RIGHT, DIST_NEAR, ANGLE_SHARP_RIGHT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_BACKRIGHT, ANGLE_RIGHT, DIST_FAR, ANGLE_SLIGHT_RIGHT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_BACKRIGHT, ANGLE_FRONT, DIST_NEAR, ANGLE_SHARP_RIGHT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_BACKRIGHT, ANGLE_FRONT, DIST_FAR, ANGLE_SHARP_RIGHT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_BACKRIGHT, ANGLE_LEFT, DIST_NEAR, ANGLE_SHARP_LEFT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_BACKRIGHT, ANGLE_LEFT, DIST_FAR, ANGLE_SHARP_LEFT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_BACKRIGHT, ANGLE_BACKLEFT, DIST_NEAR, ANGLE_SLIGHT_LEFT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_BACKRIGHT, ANGLE_BACKLEFT, DIST_FAR, ANGLE_SHARP_LEFT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);


		inferenceRule(ANGLE_RIGHT, ANGLE_BACKRIGHT, DIST_NEAR, ANGLE_SLIGHT_LEFT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_RIGHT, ANGLE_BACKRIGHT, DIST_FAR, ANGLE_SLIGHT_LEFT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_RIGHT, ANGLE_RIGHT, DIST_NEAR, ANGLE_FRONT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_RIGHT, ANGLE_RIGHT, DIST_FAR, ANGLE_FRONT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_RIGHT, ANGLE_FRONT, DIST_NEAR, ANGLE_SLIGHT_RIGHT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_RIGHT, ANGLE_FRONT, DIST_FAR, ANGLE_SLIGHT_RIGHT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_RIGHT, ANGLE_LEFT, DIST_NEAR, ANGLE_SHARP_RIGHT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_RIGHT, ANGLE_LEFT, DIST_FAR, ANGLE_SHARP_RIGHT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_RIGHT, ANGLE_BACKLEFT, DIST_NEAR, ANGLE_SLIGHT_RIGHT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S); //*
		inferenceRule(ANGLE_RIGHT, ANGLE_BACKLEFT, DIST_FAR, ANGLE_SHARP_RIGHT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);


		inferenceRule(ANGLE_FRONT, ANGLE_BACKRIGHT, DIST_NEAR, ANGLE_SHARP_LEFT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_FRONT, ANGLE_BACKRIGHT, DIST_FAR, ANGLE_SHARP_LEFT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_FRONT, ANGLE_RIGHT, DIST_NEAR, ANGLE_SHARP_LEFT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_FRONT, ANGLE_RIGHT, DIST_FAR, ANGLE_SLIGHT_LEFT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_FRONT, ANGLE_FRONT, DIST_NEAR, ANGLE_FRONT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_FRONT, ANGLE_FRONT, DIST_FAR, ANGLE_FRONT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_FRONT, ANGLE_LEFT, DIST_NEAR, ANGLE_SHARP_LEFT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_FRONT, ANGLE_LEFT, DIST_FAR, ANGLE_SLIGHT_RIGHT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_FRONT, ANGLE_BACKLEFT, DIST_NEAR, ANGLE_SHARP_RIGHT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_FRONT, ANGLE_BACKLEFT, DIST_FAR, ANGLE_SHARP_RIGHT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);


		inferenceRule(ANGLE_LEFT, ANGLE_BACKRIGHT, DIST_NEAR, ANGLE_SLIGHT_LEFT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S); //*
		inferenceRule(ANGLE_LEFT, ANGLE_BACKRIGHT, DIST_FAR, ANGLE_SHARP_LEFT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_LEFT, ANGLE_RIGHT, DIST_NEAR, ANGLE_SHARP_LEFT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_LEFT, ANGLE_RIGHT, DIST_FAR, ANGLE_SHARP_LEFT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_LEFT, ANGLE_FRONT, DIST_NEAR, ANGLE_SLIGHT_LEFT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_LEFT, ANGLE_FRONT, DIST_FAR, ANGLE_SLIGHT_LEFT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_LEFT, ANGLE_LEFT, DIST_NEAR, ANGLE_FRONT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_LEFT, ANGLE_LEFT, DIST_FAR, ANGLE_FRONT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_LEFT, ANGLE_BACKLEFT, DIST_NEAR, ANGLE_SLIGHT_RIGHT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_LEFT, ANGLE_BACKLEFT, DIST_FAR, ANGLE_SLIGHT_RIGHT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);


		inferenceRule(ANGLE_BACKLEFT, ANGLE_BACKRIGHT, DIST_NEAR, ANGLE_SHARP_RIGHT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_BACKLEFT, ANGLE_BACKRIGHT, DIST_FAR, ANGLE_SLIGHT_RIGHT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_BACKLEFT, ANGLE_RIGHT, DIST_NEAR, ANGLE_SHARP_RIGHT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_BACKLEFT, ANGLE_RIGHT, DIST_FAR, ANGLE_SHARP_RIGHT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_BACKLEFT, ANGLE_FRONT, DIST_NEAR, ANGLE_SHARP_LEFT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_BACKLEFT, ANGLE_FRONT, DIST_FAR, ANGLE_SHARP_LEFT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_BACKLEFT, ANGLE_LEFT, DIST_NEAR, ANGLE_SHARP_LEFT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_BACKLEFT, ANGLE_LEFT, DIST_FAR, ANGLE_SLIGHT_LEFT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		inferenceRule(ANGLE_BACKLEFT, ANGLE_BACKLEFT, DIST_NEAR, ANGLE_SLIGHT_LEFT, SPEED_SLOW, x_a, x_s, &Y_A, &Y_S);
		inferenceRule(ANGLE_BACKLEFT, ANGLE_BACKLEFT, DIST_FAR, ANGLE_SLIGHT_LEFT, SPEED_FAST, x_a, x_s, &Y_A, &Y_S);

		sumXY_A += x_a * Y_A;
		sumY_A += Y_A;
		sumXY_S += x_s * Y_S;
		sumY_S += Y_S;
	}

	outAngle = sumXY_A / sumY_A;
	outSpeed = sumXY_S / sumY_S;

	#ifdef VERBOSE
	printf("\n angle: sumXY_A: %.4f ; sumY_A %.4f\n", sumXY_A, sumY_A);
	printf(" speed: sumXY_S: %.4f ; sumY_S %.4f\n", sumXY_S, sumY_S);
	printf("\n!! out: a: %.4f ; %.4f\n---\n\n", outAngle*180/M_PI, outSpeed);
	#endif
}

void collisionAvoidance()
{
	static float oldX = 0, oldY = 0;
	static int samePosCount = 0, panicModeCount = 0, panicModeStart = 200;

	float cDist = environment.getCollision();
	if (cDist < 4) cDist = 4;
	float cAngle = environment.getObstacleAngle();
	environm::soccer::robotBox myRobot = environment.getOwnRobot();

	float avoidDistance = 100;
	float avoidBackAngle = 0.9;
	float avoidAngleSurround = (M_PI * 0.4);
	float avoidAngle = (M_PI * 0.3);
	float avoidAngleFront = (M_PI * 0.21);

	if (cDist < avoidDistance) 
	{
		if ((cAngle > -avoidAngleSurround) && (cAngle < avoidAngleSurround)) 
		{
			if (cAngle < 0) 
			{
				leftMotor *= ((cDist / avoidDistance) * 1);
			}
			else 
			{
				rightMotor -= ((cDist / avoidDistance) * 1);
			}
		}
		if ((cAngle > -avoidAngle) && (cAngle < avoidAngle)) 
		{
			if (ballAngle > 0) 
			{
				leftMotor *= ((cDist / avoidDistance) * 2);
			}
			else 
			{
				rightMotor *= ((cDist / avoidDistance) * 2);
			}
		}
		if ((cAngle > -avoidAngleFront) && (cAngle < avoidAngleFront)) 
		{
			if (ballAngle > 0) 
			{
				leftMotor -= ((cDist / avoidDistance)*8);
				rightMotor += ((cDist / avoidDistance) * 2);
			}
			else 
			{
				rightMotor -= ((cDist / avoidDistance) * 8);
				leftMotor += ((cDist / avoidDistance) * 2);
			}
		}
	}

	if ((abs(myRobot.pos.x - oldX) < 1.6) && (abs(myRobot.pos.y - oldY) < 1.6)) 
	{
		samePosCount++;
		if (samePosCount > 250) 
		{
			samePosCount = 0;
			panicModeCount = panicModeStart;
		}
	}
	else 
	{
		samePosCount = 0;
	}
	oldX = myRobot.pos.x;
	oldY = myRobot.pos.y;
	if (panicModeCount-- > 0) 
	{
		log_reset();
		leftMotor = (sin(((float)panicModeCount / (float)panicModeStart) * M_PI * 3) + 1) * 0.2f * -MAX_ABS_SPEED * 0.85f;
		rightMotor = (cos(((float)panicModeCount / (float)panicModeStart) * M_PI * 3) + 1) * 0.2f * -MAX_ABS_SPEED * 0.85f;
	}
}
