#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    int size = 1024, pos;
    int c;
    int w = 0;
    char *buffer = (char *)malloc(size);

    int train_number = atoi(argv[1]);
    int test_number = atoi(argv[2]);

    int current_line = 0;

    FILE* f1 = fopen("log1024.txt", "r");
    FILE* f2 = fopen("log1025.txt", "r");
    
    FILE* f_learn = fopen("majohn.lrn", "w");
    FILE* f_test = fopen("majohn.tst", "w");

    if(f1 && f2 && f_learn && f_test) {
      fprintf(f_learn, "3 2 %d\n", train_number);
      fprintf(f_test, "3 2 %d\n", test_number);
      
      do { // read all lines in file
        pos = 0;

        // will be reading spaces, add to buffer only 0, 1, 2, 8 and 9
        int i_index = 0;

        do { // read one line
          if (w == 0) {
            c = fgetc(f1);
          } else {
            c = fgetc(f2);
          }
          
          if(c != EOF) {
            if (c == ' ') i_index ++;
            if (i_index == 0 || i_index == 1 || i_index == 2 || i_index == 8 || i_index == 9)
            buffer[pos++] = (char)c;
          }

          if(pos >= size - 1) { // increase buffer length - leave room for 0
            size *=2;
            buffer = (char*)realloc(buffer, size);
          }
        } while(c != EOF && c != '\n');

        if (w == 0) {
          w = 1;
        } else {
          w = 0;
        }

        buffer[pos] = 0;
        
        if (current_line < train_number) {
            fprintf(f_learn, "%s", buffer);
        }

        if (current_line >= train_number && current_line < train_number + test_number) {
            fprintf(f_test, "%s", buffer);
        }

        current_line++;

      } while(c != EOF); 

      fclose(f1);
      fclose(f2);
      fclose(f_learn);
      fclose(f_test);
    }

    free(buffer);
    return 0;
}